class Banco {
    constructor(nome, agencia, endereco) {
        this.nome = nome
        this.agencia = agencia
        this.endereco = endereco
        this.filaClientes = []
    }

    entrouNaFila(cliente){
        this.filaClientes = [...this.filaClientes, cliente]

        console.log("Novo cliente entrou na fila!")
    }

    saiuDaFila(){
        const [_, ...outrosClientes] = this.filaClientes
        this.filaClientes = outrosClientes

        console.log("Cliente saiu da fila. Pronto para ser atendido!")
    }

    mostraFila(){
        console.log(this.filaClientes)
    }
}

module.exports = Banco