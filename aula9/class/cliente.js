const {readDB, writeDB} = require('../functions/functions')

class Cliente{
    constructor(nome, documento, telefone, email){
        this._nome = nome
        this._documento = documento
        this._telefone = telefone
        this._email = email
    }

    get nome(){
        return this._nome
    }

    set nome(nome){
        this._nome = nome
    }

    get documento(){
        return this._documento
    }

    set documento(documento){
        this._documento = documento
    }

    get telefone(){
        return this._telefone
    }

    set telefone(telefone){
        this._telefone =  telefone
    }

    get email(){
        return this._email
    }

    set email(email){
        this._email = email
    }

    contratarAdvogado(advogado){
        console.log("Advogado contratado!")
    }

    insertcliente(){
        let db = readDB()
        let newCliente
        
        if (!db.cliente){
            newCliente = {
                nome: this.nome,
                documento: this.documento,
                telefone: this.telefone,
                email: this.email
            }
            writeDB({...db, cliente: newCliente})
        }else{
            this.nome = db.cliente.nome
            this.documento = db.cliente.documento
            this.telefone = db.cliente.nroConselho
            this.email = db.cliente.especialidade
        }

        return this
    }
}

module.exports = Cliente