const Estacionamento = require('./estacionamento');

const est1 = new Estacionamento();

est1.mostraAndares();
est1.estacionar("ABC123");
est1.estacionar("ABC124");
est1.mostraAndares();

est1.estacionar("ABC125");
est1.estacionar("ABC126");
est1.mostraAndares();

est1.liberaVaga(1);
est1.estacionar("ABC127");
est1.mostraAndares();

est1.estacionar("ABC128");
est1.estacionar("ABC129");
est1.estacionar("ABC130");
est1.estacionar("ABC131");
est1.estacionar("ABC132");

est1.mostraAndares();

est1.estacionar("ABC133");

est1.liberaVaga(3);
est1.liberaVaga(2);

est1.estacionar("ABC133");

est1.mostraAndares();