class Conta {
    constructor(saldo, senha) {
        let _saldo = saldo;
        let _senha = senha;
        let autenticado = false;

        this.login = (senha) => {
            if (autenticado) {
                console.log("Usuário já autenticado");
                return;
            }

            if (senha == _senha) {
                autenticado = true;
                console.log("Login realizado com sucesso!");
            }
        }

        this.getSaldo = () => {
            if (autenticado) {
                return _saldo;
            }

            console.log("Para acessar o saldo é necessário efetuar o login!");
            return null;
        }

        this.setSaldo = (novoSaldo) => {
            if (autenticado) {
                _saldo = novoSaldo;
                return;
            }

            console.log("Para alterar o saldo é necessário efetuar o login!");
        }
    }

    get saldo() {
        return this.getSaldo();
    }

    set saldo(novoSaldo) {
        this.setSaldo(novoSaldo);
    }

    depositar(valor) {
        this.saldo = this.saldo + valor;
    }

    sacar(valor) {
        this.saldo = this.saldo - valor;
    }
}

let conta1 = new Conta(1500, "abobrinha");

conta1.saldo = 300;
conta1.login("abobrinha");
console.log(conta1.saldo);
conta1.saldo = 1700;
console.log(conta1.saldo);
conta1.setSaldo(1800);
console.log(conta1.getSaldo());

conta1.depositar(400);
console.log(conta1.saldo);

conta1.sacar(1600);
console.log(conta1.saldo);