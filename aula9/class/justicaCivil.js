const Justica = require('./justica')

class JusticaCivil extends Justica{
    constructor(processo){
        super(processo)
    }

    encerrarProcesso(sentenca, valorSentenca){
        this.processo.sentenca = sentenca
        this.processo.valorSentenca = valorSentenca
    }
}

module.exports = JusticaCivil