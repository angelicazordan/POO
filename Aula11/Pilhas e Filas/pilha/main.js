const Estante = require('./estante.js')
const Livro = require('./livro.js')

const estanteDoGabriel = new Estante([])
const biblia = new Livro("Bíblia", "Nova Jerusalém", 502)
const processaProf = new Livro("Como Processar o meu Professor", "Direito do Aluno", 5)
const casmurro = new Livro("Dom Casmurro", "Livros Clássicos", 7)
const coaching = new Livro("Desenvolvimento Pessoal - O Guia Definitivo", "Coach Quântico", 3)
const javascript = new Livro("Conceitos Avançados de Javascript", "Bom Programador", 1)

estanteDoGabriel.empilharLivro(biblia)
estanteDoGabriel.empilharLivro(javascript)
estanteDoGabriel.empilharLivro(processaProf)
estanteDoGabriel.empilharLivro(casmurro)
estanteDoGabriel.empilharLivro(coaching)

estanteDoGabriel.mostraLivros()

estanteDoGabriel.desempilharLivro()

estanteDoGabriel.mostraLivros()

estanteDoGabriel.exportarLivros()