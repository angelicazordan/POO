class Estacionamento {
    constructor(primeiroAndar=[], segundoAndar=[], terceiroAndar=[]){
        let _primeiroAndar = primeiroAndar;
        let _segundoAndar = segundoAndar;
        let _terceiroAndar = terceiroAndar;

        this.getPrimeiroAndar = () => {
            return _primeiroAndar;
        }

        this.getSegundoAndar = () => {
            return _segundoAndar;
        }

        this.getTerceiroAndar = () => {
            return _terceiroAndar;
        }

        this.setPrimeiroAndar = (primeiroAndar) => {
            _primeiroAndar = primeiroAndar;
        }

        this.setSegundoAndar = (segundoAndar) => {
            _segundoAndar = segundoAndar;
        }

        this.setTerceiroAndar = (terceiroAndar) => {
            _terceiroAndar = terceiroAndar;
        }
    }

    get primeiroAndar() {
        return this.getPrimeiroAndar();
    }

    get segundoAndar() {
        return this.getSegundoAndar();
    }

    get terceiroAndar() {
        return this.getTerceiroAndar();
    }

    set primeiroAndar(primeiroAndar) {
        this.setPrimeiroAndar(primeiroAndar);
    }

    set segundoAndar(segundoAndar) {
        this.setSegundoAndar(segundoAndar);
    }

    set terceiroAndar(terceiroAndar) {
        this.setTerceiroAndar(terceiroAndar);
    }

    adicionarCarroAoAndar(fila, placa) {
        fila = [...fila, placa]
        // fila.push(placa)
        return fila;
    }

    retirarCarroDoAndar(fila) {
        const [carroSaindo, ...restoDaFila] = fila;
        fila = restoDaFila;

        return {fila, placaRetirada: carroSaindo};
    }

    liberaVaga(andar) {
        let obj;

        switch(andar) {
            case 1:
                obj = this.retirarCarroDoAndar(this.primeiroAndar);
                this.primeiroAndar = obj.fila;
                break;
            case 2:
                obj = this.retirarCarroDoAndar(this.segundoAndar);
                this.segundoAndar = obj.fila;
                break;
            case 3:
                obj = this.retirarCarroDoAndar(this.terceiroAndar);
                this.terceiroAndar = obj.fila;
                break;
            default:
                console.log("Andar inexistente!");
                break;
        }

        if (obj.placaRetirada) {
            console.log(`Carro de placa ${obj.placaRetirada} saindo do ${andar}º andar.`);
        }
    }

    estacionar(novaPlaca){
        const MAX_VAGAS_POR_ANDAR = 3;
        let mensagem = `Carro com placa ${novaPlaca} adicionado ao`;

        if (this.primeiroAndar.length < MAX_VAGAS_POR_ANDAR) {
            this.primeiroAndar = this.adicionarCarroAoAndar(this.primeiroAndar, novaPlaca);
            mensagem += " primeiro andar"
        } else if (this.segundoAndar.length < MAX_VAGAS_POR_ANDAR) {
            this.segundoAndar = this.adicionarCarroAoAndar(this.segundoAndar, novaPlaca);
            mensagem += " segundo andar"
        } else if (this.terceiroAndar.length < MAX_VAGAS_POR_ANDAR) {
            this.terceiroAndar = this.adicionarCarroAoAndar(this.terceiroAndar, novaPlaca);
            mensagem += " terceiro andar"
        } else {
            mensagem = "Todos os andares estão LOTADOS! Procure uma vaga mais tarde."
        }

        console.log(mensagem);
    }

    mostraAndares() {
        console.log("Primeiro Andar:", this.primeiroAndar);
        console.log("Segundo Andar:", this.segundoAndar);
        console.log("Terceiro Andar:", this.terceiroAndar);
    }
}

module.exports = Estacionamento