class Conta {
    constructor(agencia, numero, saldo, senha) {
        // Atributos privados
        let _saldo = saldo;
        let _senha = senha;
        let autenticado = false;

        // Atributos públicos
        this.agencia = agencia;
        this.numero = numero;

        this.login = (senha) => {
            if (autenticado) {
                console.log("Usuário já autenticado");
                return;
            }

            if (senha == _senha) {
                autenticado = true;
                console.log("Login realizado com sucesso!");
            }
        }

        this.getSaldo = () => {
            if (autenticado) {
                return _saldo;
            }

            console.log("Para acessar o saldo é necessário efetuar o login!");
        }

        this.setSaldo = (novoSaldo) => {
            if (autenticado) {
                _saldo = novoSaldo;
                return;
            }

            console.log("Para alterar o saldo é necessário efetuar o login!");
        }
    }

    get saldo() {
        return this.getSaldo();
    }

    set saldo(novoSaldo) {
        this.setSaldo(novoSaldo);
    }

    depositar(valor) {
        this.saldo = this.saldo + valor;
    }

    sacar(valor) {
        this.saldo = this.saldo - valor;
    }
}

class ContaCorrente extends Conta {
    constructor(agencia, numero, saldo, senha, limiteCredito) {
        super(agencia, numero, saldo, senha);

        let _limiteCredito = limiteCredito;

        this.getLimiteCredito = () => {
            return _limiteCredito;
        }

        this.setLimiteCredito = (limite) => {
            _limiteCredito = limite;
        }
    }

    get limiteCredito() {
        return this.getLimiteCredito();
    }

    set limiteCredito(limite) {
        this.setLimiteCredito(limite);
    }

    sacar(valor) {
        let saldoAtual = this.saldo;
        if (saldoAtual == null) return;

        let diferenca = saldoAtual - valor;
        if (diferenca >= -this.limiteCredito) {
            this.saldo = diferenca;
            return;
        }

        console.log("Não é possível sacar além do seu limite de crédito!");
    }

    calcularJurosLimite(){
        const juros = 0.08;

        if (this.saldo < 0) {
            let valorJuros = - this.saldo * juros;
            this.saldo -= valorJuros;
            return valorJuros;
        }

        return 0;
    }
}

class ContaPoupanca extends Conta {
    constructor(agencia, numero, saldo, senha, rendimento) {
        super(agencia, numero, saldo, senha)

        let _rendimento = rendimento;

        this.getRendimento = () => {
            return _rendimento;
        }

        this.setRendimento = (rendimento) => {
            _rendimento = rendimento;
        }
    }

    get rendimento() {
        return this.getRendimento();
    }

    set rendimento(rendimento) {
        this.setRendimento(rendimento);
    }

    sacar(valor) {
        let saldoAtual = this.saldo;
        if (saldoAtual == null) return;

        let diferenca = saldoAtual - valor;
        if (diferenca >= 0) {
            this.saldo = diferenca;
            return;
        }

        console.log("Não é possível sacar valores acima do seu saldo atual!");
    }

    calcularRendimentoPeriodo() {
        const taxa = this.rendimento/100;
        let valorJuros = this.saldo * taxa;

        this.saldo += this.saldo * taxa;

        return valorJuros;
    }
}

class Pessoa {
    constructor(nome, telefone, endereco, email, contas) {
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
        this.endereco = endereco;

        let _contas = contas;

        this.getContas = () => {
            return _contas;
        }

        this.setContas = (contas) => {
            _contas = contas;
        }
    }

    get contas() {
        return this.getContas();
    }

    set contas(contas) {
        this.setContas(contas);
    }

    abrirConta(agencia, numero, saldo, senha, corrente = true) {
        let conta;

        if (corrente) {
            conta = new ContaCorrente(agencia, numero, saldo, senha, 300);
        } else {
            conta = new ContaPoupanca(agencia, numero, saldo, senha, 0.7);
        }

        this.contas.push(conta);
    }

    encerrarConta(id) {
        let saldo = this.contas[id].saldo;
        if (saldo == 0) {
            this.contas.splice(id, 1);
            return;
        } else if (saldo > 0) {
            console.log("Saque o valor total da sua conta antes de encerrá-la!");
        } else {
            console.log("Pague as dívidas da sua conta antes de encerrá-la!");
        }
    }

    solicitarCartaoDebito() {
        console.log("Cartão de Débito solicitado com sucesso. Parabéns!");
    }
}

// Testes para a Conta Corrente
// let contaCorr1 = new ContaCorrente(2333, 766127, 5000, "123456", 300);

// console.log(contaCorr1);

// contaCorr1.login("123456");

// console.log(contaCorr1.saldo);
// contaCorr1.sacar(300);
// console.log(contaCorr1.saldo);
// console.log(contaCorr1.calcularJurosLimite());

// contaCorr1.sacar(2500);
// console.log(contaCorr1.saldo);
// contaCorr1.sacar(2300);
// console.log(contaCorr1.saldo);
// console.log(contaCorr1.calcularJurosLimite());
// console.log(contaCorr1.saldo);

// Testes para a Conta Poupança
// let contaPoup = new ContaPoupanca(2333, 766127, 5000, "123456", 0.70);

// contaPoup.login("123456");

// console.log(contaPoup.saldo);
// contaPoup.sacar(1000);
// console.log(contaPoup.saldo);
// contaPoup.sacar(20000);
// console.log(contaPoup.saldo);
// contaPoup.depositar(200);
// console.log(contaPoup.saldo);

// console.log(contaPoup.calcularRendimentoPeriodo());
// console.log(contaPoup.saldo);

// Testes para a Pessoa e suas Contas
let pessoa = new Pessoa("Luís Fernando", "(21)999222333", "Avenida das Couves, 008", "lfernandotexbicalho@hotmail.com", []);

console.log(pessoa);

console.log(pessoa.contas);
pessoa.abrirConta(2333, 766127, 1000, "123456");
console.log(pessoa.contas);
pessoa.abrirConta(2333, 766127, 2500, "123456", false);
console.log(pessoa.contas);

let contaCorr = pessoa.contas[0];
let contaPoup = pessoa.contas[1];

contaCorr.login("123456");
console.log(contaCorr.saldo);
contaPoup.login("123456");
console.log(contaPoup.saldo);

let saque = contaPoup.saldo;
contaPoup.sacar(saque);
pessoa.encerrarConta(1);

console.log(contaCorr.saldo);
contaCorr.depositar(saque);
console.log(contaCorr.saldo);