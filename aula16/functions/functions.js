const fs = require('fs')

function readDB(){
    let dataDB = JSON.parse(fs.readFileSync('db/db.json'))
    return dataDB
}

function writeDB(data){
    fs.writeFileSync('db/db.json', JSON.stringify(data))
}

module.exports = {readDB, writeDB}