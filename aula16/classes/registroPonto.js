class RegistroPonto{
    constructor(horario, tipo){
        this._horario = horario
        this._tipo = tipo
    }

    get horario(){
        return this._horario
    }

    set horario(horario){
        this._horario = horario
    }

    get tipo(){
        return this._tipo
    }

    set tipo(tipo){
        this._tipo = tipo
    }

    addRegistro(){        
        let novoPonto = {
            horario: this.horario,
            tipo: this.tipo
        }
        return novoPonto
    }
}

module.exports = RegistroPonto