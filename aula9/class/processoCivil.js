const { readDB, writeDB } = require('../functions/functions')
const Processo = require('./processo')

class ProcessoCivil extends Processo{
    constructor(numero, descricao, data, partes){
        super(numero, descricao, data, partes)
        this._valorSentenca = undefined
    }

    get valorSentenca(){
        return this._valorSentenca
    }

    set valorSentenca(valor){
        this._valorSentenca = valor
    }

    inserirProcesso(){
        let db = readDB()
        let newProcesso
        
        if (!db.processo){
            newProcesso = {
                numero: this.numero, 
                descricao: this.descricao, 
                data: this.data, 
                partes: this.partes                
            }

            writeDB({...db, processo: newProcesso})
        }    
        
    }
}

module.exports = ProcessoCivil