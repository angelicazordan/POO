class Televisor {
    constructor(fabricante, modelo, canalAtual, listaCanais, volume) {
        if (isNaN(volume)) {
            this.volume = 0
        } else {
            this.volume = volume;
        }
        this.validarVolume()

        this.fabricante = fabricante;
        this.modelo = modelo;
        this.listaCanais = listaCanais;
        this.canalAtual = canalAtual;
        this.validarCanalAtual();
    }

    aumentarVolume() {
        this.volume += 1;
        this.validarVolume();
    }

    diminuirVolume() {
        this.volume -= 1;
        this.validarVolume();
    }

    validarVolume() {
        if (this.volume > 100) {
            this.volume = 100;
        } 
        else if (this.volume < 0) {
            this.volume = 0;
        }
    }

    sintonizarCanal(nomeCanal) {
        this.listaCanais.push(nomeCanal)
    }

    aumentarCanal() {
        this.canalAtual += 1;
        this.validarCanalAtual();
    }

    diminuirCanal() {
        this.canalAtual -= 1;
        this.validarCanalAtual();
    }

    validarCanalAtual() {
        if (this.canalAtual >= this.listaCanais.length) {
            this.canalAtual = 0;
        }
        else if (this.canalAtual < 0) {
            this.canalAtual = this.listaCanais.length - 1;
        }
    }
}

class ControleRemoto {
    constructor(televisao) {
        this.televisao = televisao;
    }

    aumentarVolume() {
        this.televisao.aumentarVolume();
    }

    diminuirVolume() {
        this.televisao.diminuirVolume();
    }

    aumentarCanal() {
        this.televisao.aumentarCanal();
    }

    diminuirCanal() {
        this.televisao.diminuirCanal();
    }

    sintonizarCanal(nomeCanal) {
        const posicao = this.televisao.listaCanais.indexOf(nomeCanal);

        if (posicao == -1) {
            this.televisao.sintonizarCanal(nomeCanal);
        }
    }
}

const listaCanais = [
    "Disney Channel",
    "Globo",
    "SBT",
    "Band",
    "RedeTV"
];
let tel = new Televisor("Samsung", "MX21", 0, listaCanais, 1);
let ctrlRemoto = new ControleRemoto(tel);

// Testes de volume
ctrlRemoto.diminuirVolume();
ctrlRemoto.diminuirVolume();

console.log(ctrlRemoto);

// Testes de canal
ctrlRemoto.diminuirCanal();
console.log(ctrlRemoto);

ctrlRemoto.aumentarCanal();
ctrlRemoto.aumentarCanal();
console.log(ctrlRemoto);

// Teste para sintonizar canaiså
ctrlRemoto.sintonizarCanal("Globo");
ctrlRemoto.sintonizarCanal("Globo News");
ctrlRemoto.sintonizarCanal("Fox");
ctrlRemoto.sintonizarCanal("SBT");
console.log(ctrlRemoto);