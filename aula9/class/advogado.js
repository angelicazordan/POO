const {readDB, writeDB} = require('../functions/functions')
const ProcessoCivil = require('./processoCivil')
const ProcessoCriminal = require('./processoCriminal')
const JusticaCivil = require('./justicaCivil')
const JusticaCriminal = require('./justicaCriminal')

class Advogado{
    constructor(nome, conselho, especialidade){
        this._nome = nome
        this._nroConselho = conselho
        this._especialidade = especialidade
    }

    get nome(){
        return this._nome
    }

    set nome(nome){
        this._nome = nome
    }

    get nroConselho(){
        return this._nroConselho
    }

    set nroConselho(conselho){
        this._nroConselho = conselho
    }

    get especialidade(){
        return this._especialidade
    }

    set especialidade(especialidade){
        this._especialidade = especialidade
    }

    abrirProcesso(tipoProcesso, numero, descricao, partes){
        let processo
        let justica

        switch(tipoProcesso){
            case "CIVIL":
                processo = new ProcessoCivil(numero, descricao, new Date(), partes)
                justica = new JusticaCivil(processo)
                processo.inserirProcesso()
                break
            case "CRIMINAL":
                processo = new ProcessoCriminal(numero, descricao, new Date(), partes)
                justica = new JusticaCriminal(processo)
                processo.inserirProcesso()
                break
            default:
                console.log("O tipo do processo deve ser CIVIL ou CRIMINAL.")
            
        }

        return justica

    }

    consultarProcesso(justica){
        let db = readDB()
        const newProcesso = db.processo

        console.log(newProcesso) 
    }

    insertAdvogado(){
        let db = readDB()
        let newAdvogado
        
        if (!db.advogado){
            newAdvogado = {
                nome: this.nome,
                nroConselho: this.nroConselho,
                especialidade: this.especialidade
            }
            writeDB({advogado: newAdvogado})
        }else{
            this.nome = db.advogado.nome
            this.nroConselho = db.advogado.nroConselho
            this.especialidade = db.advogado.especialidade
        }

        return this
    }

}

module.exports = Advogado