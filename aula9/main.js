const input = require('readline-sync')
const {readDB, writeDB} = require('./functions/functions')
const Advogado = require('./class/advogado')
const Cliente = require('./class/cliente')

let db = readDB()
let newAdvogado = new Advogado("angelica", "123456", "Criminal")
let newCliente = new Cliente("Flavio Almeida", "123456", "(11)54987-877", "teste@teste")

let advogado1 = newAdvogado.insertAdvogado()

let cliente1 = newCliente.insertcliente()

console.log(`Bem vindo ao sistema de processos do advogado ${advogado1.nome} - OAB ${advogado1.nroConselho}`)

console.log('Menu inicial')
console.log('1 - Criar contrato com cliente')
console.log('2 - Abrir Processo')
console.log('3 - consultar Processo')
console.log('4 - Adicionar delito e vara' )
console.log('5 - Registrar ocorrência')
console.log('6 - Encerrar Processo')
console.log('7 - Sair')

let opcaoEscolhida = parseInt(input.question('Escolha uma opção do menu:')) 

let justica

while(opcaoEscolhida !==7){
    switch(opcaoEscolhida){
        case 1:
            cliente1.contratarAdvogado(advogado1)
            break
        case 2:
            if (!justica){
                justica = advogado1.abrirProcesso("CRIMINAL", "12456787787878", "Quero processar uma pessoa", [cliente1.nome, "Angelica"])
                console.log("Processo aberto!")
            }else{
                console.log("Já existe um processo aberto. Utilize a opção 3 para consultar.")
            }
            break
        case 3:
            if (!justica){
                console.log("Não há processo aberto")
            }else{
                advogado1.consultarProcesso(justica)
            }
            break
        case 4:
            if (!justica){
                console.log("Não há processo aberto")
            }else{  
                justica.adicionarDelitoeVara("Não deixou jantar", "Fome")
                justica.atualizarInfos()
                console.log('Delito adicionado com sucesso.')
            }
            break
        case 5:
            if (!justica){
                console.log("Não há processo aberto")
            }else{
                justica.registrarOcorrencia("Poderia ter esquentado uma marmita") 
                justica.atualizarInfos()
                console.log('Ocorrência registrada com sucesso.')
            }
            break
        case 6:
            if (!justica){
                console.log("Não há processo aberto")
            }else{
                justica.encerrarProcesso("comer após a aula")
                justica.atualizarInfos()
                console.log('Processo encerrado com sucesso.')
            }
            break
        case 7:
            console.clear()
            break
        default:
            console.log('Opção inválida')
            break
    }

    opcaoEscolhida = parseInt(input.question('Escolha uma opção do menu:')) 
}