const input = require('readline-sync')
const Banco = require('./banco')
const Cliente = require('./cliente')

const banco = new Banco("Itaú", 671, "Avenida das Couves, 200")

console.log("Entre com os dados de cada cliente na fila")

let nome = input.question("Digite o seu nome: ")

while (nome != "FIM") {
    const idade = parseInt(input.question("Digite a sua idade: "))
    const cliente = new Cliente(nome, idade)

    banco.entrouNaFila(cliente)

    const atendido = input.question("Cliente atendido?")

    if (atendido == 1) {
        banco.saiuDaFila()
    }

    nome = input.question("Digite o seu nome: ")
}

banco.mostraFila()

console.log("Agência fechada!")