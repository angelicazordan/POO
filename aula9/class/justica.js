const { writeDB, readDB} = require('../functions/functions')
class Justica{
    constructor(processo){
        this._processo = processo
    }

    get processo(){
        return this._processo
    }

    registrarOcorrencia(ocorrencia){
        this.processo.descricao += "\n" + ocorrencia
    }

    encerrarProcesso(sentenca){
        this.processo.sentenca = sentenca
    }

    atualizarInfos(){
        let db = readDB()

        const newProcesso = {...db.processo, 
            descricao: this.processo.descricao, 
            sentenca: this.processo.sentenca}

        writeDB({...db, processo: newProcesso})
    }
}

module.exports = Justica