class Televisor {
    constructor(fabricante, modelo, canalAtual, listaCanais, volume) {
        // Possibilidade de Tratamento 1
        if (isNaN(volume)) {
            this.volume = 0
        } else {
            this.volume = volume;
        }
        this.validarVolume()

        // Possibilidade de Tratamento 2
        // if (isNaN(volume)) {
        //     throw "Volume deve possuir valor estritamente numérico";
        // }
        
        this.fabricante = fabricante;
        this.modelo = modelo;
        this.listaCanais = listaCanais;
        this.canalAtual = canalAtual;
        this.validarCanalAtual();
        // this.volume = volume;
    }

    aumentarVolume() {
        this.volume += 1;
        this.validarVolume();
    }

    diminuirVolume() {
        this.volume -= 1;
        this.validarVolume();
    }

    validarVolume() {
        if (this.volume > 100) {
            this.volume = 100;
        } 
        else if (this.volume < 0) {
            this.volume = 0;
        }
    }

    sintonizarCanal(nomeCanal) {
        this.listaCanais.push(nomeCanal)
    }

    aumentarCanal() {
        this.canalAtual += 1;
        this.validarCanalAtual();
    }

    diminuirCanal() {
        this.canalAtual -= 1;
        this.validarCanalAtual();
    }

    validarCanalAtual() {
        if (this.canalAtual >= this.listaCanais.length) {
            this.canalAtual = 0;
        }
        else if (this.canalAtual < 0) {
            this.canalAtual = this.listaCanais.length - 1;
        }
    }
}

let tel1 = new Televisor("Samsung", "MX21", 0, [], 1);
console.log(tel1);

tel1.diminuirVolume();
tel1.diminuirVolume();
console.log(tel1);

tel1.sintonizarCanal("Disney Channel");
tel1.sintonizarCanal("Globo");
tel1.sintonizarCanal("SBT");
tel1.sintonizarCanal("Band");
tel1.sintonizarCanal("RedeTV");
console.log(tel1);

tel1.diminuirCanal();
tel1.diminuirCanal();
tel1.diminuirCanal();
tel1.diminuirCanal();
tel1.diminuirCanal();
tel1.diminuirCanal();
console.log(tel1);