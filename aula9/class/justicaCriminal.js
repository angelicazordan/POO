const { writeDB, readDB} = require('../functions/functions')
const Justica = require('./justica')

class JusticaCriminal extends Justica{
    constructor(processo){
        super(processo)
    }

    adicionarDelitoeVara(delito, tipoVara){
        this.processo.delito = delito
        this.processo.tipoVara = tipoVara
    }

    atualizarInfos(){
        let db = readDB()

        const newProcesso = {...db.processo, 
            descricao: this.processo.descricao, 
            sentenca: this.processo.sentenca,
            delito: this.processo.delito,
            vara: this.processo.tipoVara}

        writeDB({...db, processo: newProcesso})
    }
}

module.exports = JusticaCriminal