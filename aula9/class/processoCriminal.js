const { readDB, writeDB } = require('../functions/functions')
const Processo = require('./processo')

class ProcessoCriminal extends Processo{
    constructor(numero, descricao, data, partes, delito, tipoVara){
        super(numero, descricao, data, partes)
        this._delito = delito
        this._tipoVara = tipoVara
    }    

    get delito(){
        return this._delito
    }

    set delito(delito){
        this._delito = delito
    }


    get tipoVara(){
        return this._tipoVara
    }

    set tipoVara(tipoVara){
        this._tipoVara = tipoVara
    }

    inserirProcesso(){
        let db = readDB()
        let newProcesso
        
        if (!db.processo){
            newProcesso = {
                numero: this.numero, 
                descricao: this.descricao, 
                data: this.data, 
                partes: this.partes, 
                delito: this.delito, 
                tipoVara: this.tipoVara
            }

            writeDB({...db, processo: newProcesso})
        }    
    }

}

module.exports = ProcessoCriminal