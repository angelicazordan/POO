const Funcionario = require('./classes/funcionario')
const input = require('readline-sync')
const { readDB } = require('../aula9/functions/functions')

while(true){

    let db = readDB()
    let funcionario = 

    console.log("Bem vindo ao sistema de ponto. Efetue seu login para registrar seus dados.")

    let email = input.question("Informe seu email:")
    let senha = input.question("Informe sua senha:")

    if (!db.funcionario){
        let cadastrar = input.question("Deseja cadastrar o novo funcionário com os dados informados? (s/n): ")

        if (cadastrar == "s"){
            let nome = input.question("Informe o nome do funcionario: ")
            funcionario = new Funcionario(nome, email, senha)
            funcionario.salvarFuncionario()
        }
    }else{
        funcionario = new Funcionario(db.funcionario.nome, db.funcionario.email, db.funcionario.senha)
        funcionario.verificaLogin(email, senha)        
    }

    if (funcionario.logado){

        let menuFunc = 1

        while (menuFunc != 0) {
            console.log(`Olá ${funcionario.nome}, escolha sua opção:
            1. Registrar Ponto
            2. Consultar Registros
            3. consultar Banco de horas
            0. Sair`)

            menuFunc = parseInt(input.question("Opção escolhida: "))

            switch (menuFunc){
                case 1:
                    funcionario.registrarPonto(new Date())
                    console.log("Ponto registrado com sucesso!")
                    break
                case 2:
                    console.log("Lista de horários registrados:")    
                    console.log(funcionario.consultarRegistrosPonto())
                    break
                case 3:
                    console.log("Quantidade de horas trabalhadas:")    
                case 0:
                    console.log("Encerrando a sessão.")
                    console.clear
                    break
                default:
                    console.log("Escolha uma opção válida.")
                    break
            }
        }
       
    }else{
        console.log("Dados de login inválidos, verifique.")
    }

}