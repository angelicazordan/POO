const fs = require('fs')

class Estante {
    constructor(livros) {
        this.livros = livros
    }

    empilharLivro(livro) {
        this.livros = [livro, ...this.livros]

        console.log(`Livro "${livro.nome}" empilhado com sucesso!`)
    }

    desempilharLivro(){
        const [livroRemovido, ...livrosRestantes] = this.livros
        this.livros = livrosRestantes

        console.log(`Livro "${livroRemovido.nome}" desempilhado!`)
    }

    mostraLivros(){
        console.log("Os livros empilhados são: ", this.livros)
    }

    exportarLivros(){
        fs.writeFileSync("livros.json", JSON.stringify(this.livros))
    }
}

module.exports = Estante