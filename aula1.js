let agenda = []

let nome = 'Angelica'

let contato1 = {
    nome: 'Angelica',
    celular: '(11) 99999-5465'
}


class Contato {
    constructor(nome, celular, idade){
        if (isNaN(idade)) throw 'Atenção! A idade precisa obrigatoriamente ser numérica!'

        this.nome = nome
        this.celular = celular
        this.email = undefined
        this.idade = idade
    }
}

let contato2 = new Contato('Luiz Fernando', '(21) 88888-1425', '20')

console.log(agenda)
console.log(nome)
console.log(contato1)
console.log(contato2)

nome = 'Angelica Zordan'
contato2.nome = 'Luís Fernando'
contato2.email = 'luisfernando@letscode.com.br'

console.log(nome)
console.log(contato2)
console.log('Este é o celular do contato 2:', contato2.celular)

agenda.push(contato1)
agenda.push(contato2)

console.log( 'Esta é a agenda completa: \n', agenda)