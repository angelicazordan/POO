class Televisor {
    constructor(fabricante, modelo, canalAtual, listaCanais, volume) {        
        this._fabricante = fabricante;
        this._modelo = modelo;
        this._listaCanais = listaCanais;
        this.canalAtual = canalAtual;
        this.volume = volume;        
    }

    get fabricante(){
        return this._fabricante;
    }

    get modelo(){
        return this._modelo;
    }

    get volume(){
        return this._volume;
    }

    set volume(volume){
        if (isNaN(volume)) {
            this._volume = 0
        } else {
            this._volume = volume;
        }
        this.validarVolume()
    }

    get listaCanais() {
        return this._listaCanais;
    }

    set listaCanais(listaCanais){
        this._listaCanais = listaCanais
    }

    get canalAtual(){
        return this._canalAtual;
    }

    set canalAtual(canal){
        this._canalAtual = canal;
        this.validarCanalAtual();
    }

    aumentarVolume() {
       this.volume = this._volume + 1; 
    }

    diminuirVolume() {
        this.volume = this._volume - 1;
        
    }

    validarVolume() {
        if (this.volume > 100) {
            this.volume = 100;
        } 
        else if (this.volume < 0) {
            this.volume = 0;
        }
    }

    sintonizarCanal(nomeCanal) {
        this.listaCanais.push(nomeCanal)
    }

    aumentarCanal() {
       this.canalAtual = this.canalAtual + 1;
        
    }

    diminuirCanal() {
       this.canalAtual = this.canalAtual - 1;
       
    }

    validarCanalAtual() {
        if (this.canalAtual >= this.listaCanais.length) {
            this.canalAtual = 0;
        }
        else if (this.canalAtual < 0) {
            this.canalAtual = this.listaCanais.length - 1;
        }
    }
}

class ControleRemoto {
    constructor(televisao) {
        this._televisao = televisao;
    }

    get televisao(){
       return this._televisao;
    }

    aumentarVolume() {
        this.televisao.aumentarVolume();
    }

    diminuirVolume() {
        this.televisao.diminuirVolume();
    }

    aumentarCanal() {
        this.televisao.aumentarCanal();
    }

    diminuirCanal() {
        this.televisao.diminuirCanal();
    }

    sintonizarCanal(nomeCanal) {
        const posicao = this.televisao.listaCanais.indexOf(nomeCanal);

        if (posicao == -1) {
            this.televisao.sintonizarCanal(nomeCanal);
        }
    }
}

const listaCanais = [
    "Disney Channel",
    "Globo",
    "SBT",
    "Band",
    "RedeTV"
];
let minhaTV = new Televisor("Samsung", "MX21", 0, listaCanais, 1);
let ctrlRemoto = new ControleRemoto(minhaTV);

// Testes de volume
minhaTV.diminuirVolume();
minhaTV.diminuirVolume();

console.log(minhaTV);

// Testes de canal
minhaTV.diminuirCanal();
console.log(minhaTV);

minhaTV.aumentarCanal();
minhaTV.aumentarCanal();
console.log(minhaTV);

//Teste de acesso a atributos e métodos
console.log("metodo getter", minhaTV.volume)
console.log("atributo direto", minhaTV._volume)


// Teste para sintonizar canais
ctrlRemoto.sintonizarCanal("Globo");
ctrlRemoto.sintonizarCanal("Globo News");
ctrlRemoto.sintonizarCanal("Fox");
ctrlRemoto.sintonizarCanal("SBT");
console.log(ctrlRemoto);

ctrlRemoto.sintonizarCanal("POO");

console.log(ctrlRemoto);

ctrlRemoto.sintonizarCanal("POG");

console.log(ctrlRemoto);