let now = new Date() 
const dayName = ["domingo", "segunda", "terça", "quarta", "quinta", "sexta", "sábado"]
const monthName = new Array("janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro")

console.log(now)

// hoje é segunda, dia 7 de junho de 2021
console.log("Hoje é " + now.getDay() + ", dia " + now.getDate() + " de " + now.getMonth() + " de " + now.getFullYear())

console.log("Hoje é " + dayName[now.getDay()] + ", dia " + now.getDate() + " de " + monthName[now.getMonth()] + " de " + now.getFullYear())

//agora são 19 horas, 40 minutos, 35 segundos
let h = now.getHours()
let m = now.getMinutes()
let s = now.getSeconds()
let ms = now.getMilliseconds()

console.log("Agora são " + h +" horas, " + m + " minutos, " + s + " segundos," + ms + " milisegundos")

let newDay = now.setDate(10)

console.log(newDay)
console.log(now)

let newDateValue = new Date("2021-10-06 15:30:45")

console.log(newDateValue)

let birthDate = new Date(1990, 9, 6)
console.log(birthDate)

console.log(birthDate.toLocaleDateString())
console.log(birthDate.toLocaleString())
console.log(birthDate.toString())
console.log(birthDate.toTimeString())
console.log(birthDate.toJSON())

console.log(birthDate.valueOf())


/*
-----------------------------------------------------------------------------------
Método	   Descrição	     Valores retornados
-----------------------------------------------------------------------------------
getDate() 	O dia do mês	 1 a 31
-----------------------------------------------------------------------------------
getUTCDate()
-----------------------------------------------------------------------------------
getDay()	O valor inteiro do dia da semana	0 a 6
-----------------------------------------------------------------------------------
getUTCDay()
-----------------------------------------------------------------------------------
getFullYear()	O ano com quatro dígitos	1900 em diante
-----------------------------------------------------------------------------------
getUTCFullYear
-----------------------------------------------------------------------------------
getHours()	A hora do dia	0 a 23
-----------------------------------------------------------------------------------
getUTCHours()
-----------------------------------------------------------------------------------
getMilliseconds()	O número de milissegundos desde o último segundo	0 a 999
-----------------------------------------------------------------------------------
getUTCMilliseconds()
-----------------------------------------------------------------------------------
getMinutes()	O número de minutos desde a última hora	0 a 59
-----------------------------------------------------------------------------------
getUTCMinutes()
-----------------------------------------------------------------------------------
getMonth()	O mês do ano	0 a 11
-----------------------------------------------------------------------------------
getUTCMonth()
-----------------------------------------------------------------------------------
getSeconds()	O número de segundos desde o último minuto	0 a 59
-----------------------------------------------------------------------------------
getUTCSeconds()
-----------------------------------------------------------------------------------
getTime()	O número de milissegundos desde a meia noite de 1º de janeiro de 1970
-----------------------------------------------------------------------------------
getTimezoneOffset()	Diferença entre hora local e GMT em minutos	0 a 1439
-----------------------------------------------------------------------------------
getYear()	O ano da data	0 a 99 para os anos de 1900 a 1999 e quatro dígitos em diante
-----------------------------------------------------------------------------------
parse()	Dado um string de data/hora, retorna o número de milissegundos desde meia noite de 1º de jan. de 1970
-----------------------------------------------------------------------------------
setDate()	Define o dia, dado o número entre 1 e 31	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCDate()	Data em milissegundos
-----------------------------------------------------------------------------------
setFullYear()	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCFullYear()	Data em milissegundos
-----------------------------------------------------------------------------------
setHours()	Define a hora, dado um número entre 0 e 23	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCHours()
-----------------------------------------------------------------------------------
setMilliseconds()	Define os milissegundos, dado um número	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCMilliseconds()	Data em milissegundos
-----------------------------------------------------------------------------------
setMinutes()	Define os minutos, dado um número entre 0 e 59	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCMinutes()
-----------------------------------------------------------------------------------
setMonth()	Define o mês, dado um número entre 0 e 11.	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCMonth()
-----------------------------------------------------------------------------------
setSeconds()	Define os segundos, dado um número entre 0 e 59	Data em milissegundos
-----------------------------------------------------------------------------------
setUTCSeconds()
-----------------------------------------------------------------------------------
setTime()	Define uma data, dado um número de milissegundos desde janeiro de 1970	Data em milissegundos
-----------------------------------------------------------------------------------
setYear()	Define o ano, dado um número de dois ou quatro dígitos	Data em milissegundos
-----------------------------------------------------------------------------------
toGMTString()	A data e hora GMT em formato de string	Day dd mmm yyyy, hh:mm:ss GMT
-----------------------------------------------------------------------------------
toUTCString()
-----------------------------------------------------------------------------------
TolocaleString()	A data e hora local em formato de string	Varia de acordo com o SO, localidade e navegador
-----------------------------------------------------------------------------------
toString()	A data e hora local em formato de string	Varia de acordo com o SO e o navegador
-----------------------------------------------------------------------------------
UTC()	Sendo dada uma data no formato de ano, mês e dia (e horas, minutos, segundos e milissegundos opcionais), retorna o número de milissegundos desde a meia noite de 1º de jan. de 1970	Data em milissegundos
-----------------------------------------------------------------------------------
valueOf()	Número de milissegundos desde a meia noite de 1º de janeiro de 1970	Data em milissegundos
-----------------------------------------------------------------------------------

*/