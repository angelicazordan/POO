class Processo{
    constructor(numero, descricao, data, partes){
        this._numero = numero
        this._descricao = descricao
        this._data = data
        this._partes =partes
        this._sentenca = undefined
    }

    get numero(){
        return this._numero
    }

    set numero(numero){
        this._numero = numero
    }

    get descricao(){
        return this._descricao
    }

    set descricao(descricao){
        this._descricao = descricao
    }

    get data(){
        return this._data
    }

    set data(data){
        this._data = data
    }

    get partes(){
        return this._partes
    }

    set partes(partes){
        this._partes = partes
    }

    get sentenca(){
        return this._sentenca
    }

    set sentenca(sentenca){
        this._sentenca = sentenca
    }

}

class ProcessoCivil extends Processo{
    constructor(numero, descricao, data, partes){
        super(numero, descricao, data, partes)
        this._valorSentenca = undefined
    }

    get valorSentenca(){
        return this._valorSentenca
    }

    set valorSentenca(valor){
        this._valorSentenca = valor
    }

}

class ProcessoCriminal extends Processo{
    constructor(numero, descricao, data, partes, delito, tipoVara){
        super(numero, descricao, data, partes)
        this._delito = delito
        this._tipoVara = tipoVara
    }    

    get delito(){
        return this._delito
    }

    set delito(delito){
        this._delito = delito
    }


    get tipoVara(){
        return this._tipoVara
    }

    set tipoVara(tipoVara){
        this._tipoVara = tipoVara
    }

}

class Justica{
    constructor(processo){
        this._processo = processo
    }

    get processo(){
        return this._processo
    }

    registrarOcorrencia(ocorrencia){
        this.processo.descricao += "\n" + ocorrencia
    }

    encerrarProcesso(sentenca){
        this.processo.sentenca = sentenca
    }
}

class JusticaCivil extends Justica{
    constructor(processo){
        super(processo)
    }

    encerrarProcesso(sentenca, valorSentenca){
        this.processo.sentenca = sentenca
        this.processo.valorSentenca = valorSentenca
    }
}

class JusticaCriminal extends Justica{
    constructor(processo){
        super(processo)
    }

    adicionarDelitoeVara(delito, tipoVara){
        this.processo.delito = delito
        this.processo.tipoVara = tipoVara
    }
}

class Advogado{
    constructor(nome, conselho, especialidade){
        this._nome = nome
        this._nroConselho = conselho
        this._especialidade = especialidade
    }

    get nome(){
        return this._nome
    }

    set nome(nome){
        this._nome = nome
    }

    get nroConselho(){
        return this._nroConselho
    }

    set nroConselho(conselho){
        this._nroConselho = conselho
    }

    get especialidade(){
        return this._especialidade
    }

    set especialidade(especialidade){
        this._especialidade = especialidade
    }

    abrirProcesso(tipoProcesso, numero, descricao, partes){
        let processo
        let justica

        switch(tipoProcesso){
            case "CIVIL":
                processo = new ProcessoCivil(numero, descricao, new Date(), partes)
                justica = new JusticaCivil(processo)
                break
            case "CRIMINAL":
                processo = new ProcessoCriminal(numero, descricao, new Date(), partes)
                justica = new JusticaCriminal(processo)
                break
            default:
                console.log("O tipo do processo deve ser CIVIL ou CRIMINAL.")
            
        }

        return justica

    }

    consultarProcesso(justica){
        console.log(justica.processo) 
    }
}

class Cliente{
    constructor(nome, documento, telefone, email){
        this._nome = nome
        this._documento = documento
        this._telefone = telefone
        this._email = email
    }

    get nome(){
        return this._nome
    }

    set nome(nome){
        this._nome = nome
    }

    get documento(){
        return this._documento
    }

    set documento(documento){
        this._documento = documento
    }

    get telefone(){
        return this._telefone
    }

    set telefone(telefone){
        this._telefone =  telefone
    }

    get email(){
        return this._email
    }

    set email(email){
        this._email = email
    }

    contratarAdvogado(advogado){
        console.log("Advogado contratado!")
    }

}

1, "teste", "12/05/2021", ["Angelica", "Luís"]

let advogado1 = new Advogado("Gabriel", "123456", "Criminal")
let cliente1 = new Cliente("Flavio", "123456", "(11)54987-877", "teste@teste")

cliente1.contratarAdvogado(advogado1)

let justica = advogado1.abrirProcesso("CRIMINAL", "12456787787878", "Quero processar uma pessoa", [cliente1.nome, "Angelica"])

advogado1.consultarProcesso(justica)

justica.adicionarDelitoeVara("Não deixou jantar", "Fome")

advogado1.consultarProcesso(justica)

justica.registrarOcorrencia("Poderia ter esquentado uma marmita")

advogado1.consultarProcesso(justica)

justica.encerrarProcesso("comer após a aula")

advogado1.consultarProcesso(justica)