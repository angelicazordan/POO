const { readDB, writeDB } = require("../functions/functions")
const RegistroPonto = require("./registroPonto")

class Funcionario{
    constructor(nome, email, senha){
        this._nome = nome
        this._email = email
        this._senha = senha
        this._logado = false
        this._listaHorarios = []
    }

    get nome(){
        return this._nome
    }

    set nome(nome){
        this._nome = nome
    }

    get email(){
        return this._email
    }

    set email(email){
        this._email = email
    }

    get senha(){
        return this._senha
    }

    set senha(senha){
        this._senha = senha
    }

    get listaHorarios(){
        return this._listaHorarios
    }

    set listaHorarios(lista){
        this._listaHorarios = lista
    }

    get logado(){
        return this._logado
    }

    set logado(logado){
        this._logado = logado
    }

    verificaLogin(email, senha){
       console.log(this)
       
        if ((this.email === email) && (this.senha === senha)){
            this.logado = true
        }
    }

    salvarFuncionario(){
        let db = readDB()
        let newFuncionario
        
        if (!db.funcionario){
            newFuncionario = {
                nome: this.nome,
                email: this.email,
                senha: this.senha,
                listaHorarios: []
            }
            writeDB({funcionario: newFuncionario})
        }

        this.logado = true

        return this
    }

    registrarPonto(horario){
        let db = readDB()
        let tipo
        let newlistaHorarios = db.funcionario.listaHorarios
        const qtde = this.listaHorarios.length

        if (qtde == 0){
            tipo = "E"
        }else{
            tipo = this.listaHorarios[qtde -1].tipo == "E" ? "S" : "E"
        } 

        let novoRegistro = new RegistroPonto(horario, tipo)

        this.listaHorarios = [...this.listaHorarios, novoRegistro]

        newlistaHorarios = [...newlistaHorarios, novoRegistro.addRegistro()]

        db.funcionario.listaHorarios = newlistaHorarios
        
        writeDB({funcionario: db.funcionario})
    }

    consultarRegistrosPonto(){
        const db = readDB()
        return db.funcionario.listaHorarios
    }
}

module.exports = Funcionario