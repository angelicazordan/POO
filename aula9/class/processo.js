class Processo{
    constructor(numero, descricao, data, partes){
        this._numero = numero
        this._descricao = descricao
        this._data = data
        this._partes =partes
        this._sentenca = undefined
    }

    get numero(){
        return this._numero
    }

    set numero(numero){
        this._numero = numero
    }

    get descricao(){
        return this._descricao
    }

    set descricao(descricao){
        this._descricao = descricao
    }

    get data(){
        return this._data
    }

    set data(data){
        this._data = data
    }

    get partes(){
        return this._partes
    }

    set partes(partes){
        this._partes = partes
    }

    get sentenca(){
        return this._sentenca
    }

    set sentenca(sentenca){
        this._sentenca = sentenca
    }

}

module.exports = Processo