// class Cliente {
//     constructor(nome,idade,email){
//       this.nome = nome
//       this.idade = idade
//       this.email = email
//     }

//     imprimirDados(){
//         return console.log("Nome do cliente: ", this.nome, ", Idade: ", this.idade, ", email: ", this.email)
//     }
//   }
  
// const cliente1 = new Cliente('Angelica', 30, "teste@teste.com") 
// console.log("objeto sem tratamento", cliente1)
// cliente1.imprimirDados()


// class Bola {
//     constructor(cor,raio){
//       this.cor = cor
//       this.raio = raio
//       this.area = undefined
//       this.volume = undefined
//     }

//     calclularArea(){
//         return 4 * 3.14 * this.raio * this.raio
//     }

//     calcularVolume(){
//         return 4 * 3.14 * (this.raio * this.raio * this.raio)/3
//     }

//     imprimirDados(){
//         const area = this.calclularArea()
//         const volume = this.calcularVolume()

//         this.area = area
//         this.volume = volume

//         console.log("Cor da bola: ", this.cor, "Area da bola: ", this.area, "Volume da Bola: ", this.volume)
//     }
//   }
    
//   let bola1 = new Bola('vermelho',8)
//   bola1.imprimirDados()
  



class Retangulo {
    constructor (lado_a, lado_b){
      this.lado_a = lado_a
      this.lado_b = lado_b
    }

    calcularArea(){
      return this.lado_a * this.lado_b
    } 
  }
  
  const retangulo1 = new Retangulo(10,15)
  
  console.log('Area do retangulo: ', retangulo1.calcularArea())